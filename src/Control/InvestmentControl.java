package Control;

import Model.BankAccount;

public class InvestmentControl {
	private static final double INITIAL_BALANCE = 1000;   
	public BankAccount account;	
	
	public InvestmentControl(){
		account = new BankAccount(INITIAL_BALANCE);  		
	}
	
	public void deposit(double pay){
		account.deposit(pay);
	}
	public void withdraw(double pay){
		account.withdraw(pay);
	}
	public double getBalance(){
		return account.getBalance();
	}
}

/**
 ทำไมถึงเลือกให้ object ของ BankAccount และ InvestmentFrame เป็น attribute / local variable ใน class controller 
(งานแลปนี้หนูสร้าง class InvestmentGUI ขึ้นมาแทน InvestFrame นะคะ)
- เพราะเราไม่ต้องการให้ class InvestmentGUI ซึ่งเป็นส่วน GUI เข้าถึง class BankAcount ซึ่งเป็น model ได้โดยตรง เราจึงจำเป็นจะต้องมี class InvestmentControl 
   ขึ้นมาเพื่อควบคุมการทำงาน โดยที่ class InvestmentControl จะไปเรียกใช้ ข้อมูลใน class BankAccount ผ่าน reference object ที่สร้างขึ้นใน class BankAccount
   และในส่วนของ class InvestmentGUI เราก็จะมีการสร้าง reference object ของ class InvestmentControl ขึ้นมาเพื่อจะได้สามารถเรียกใช้ method ของ 
  class BankAccount ผ่านทาง class InvestmentControl เพื่อที่จะสามารถนำข้อมูล ของ class InvestmentControl มาแสดงผล 
 */
