package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import Control.InvestmentControl;

public class InvestmentGUI extends JFrame{
	private JLabel rateLabel;
	private JTextField rateField;
    private JButton button;
	private JLabel resultLabel;
	private JPanel panel;
	private InvestmentControl control;
	private static final double DEFAULT_RATE = 5;
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;	
	
	
	public InvestmentGUI(){		
		control = new InvestmentControl();
		createFrame();					
	}	
	public void createFrame(){				
		rateLabel = new JLabel("Interest Rate: ");
		final int FIELD_WIDTH = 10;
	    rateField = new JTextField(FIELD_WIDTH);
	    rateField.setText("" + DEFAULT_RATE);
	    button = new JButton("Add Interest");
	    button.addActionListener(new ListenerMgr());
	    resultLabel = new JLabel("balance: " +control.getBalance());
	    panel = new JPanel();
	    panel.add(rateLabel);
	    panel.add(rateField);
	    panel.add(button);
	    panel.add(resultLabel);      
	    add(panel);  
	    setVisible(true);
	    setSize(FRAME_WIDTH,FRAME_HEIGHT);
	}
	
	class ListenerMgr implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
				double rate = Double.parseDouble(getRateField()); 
	    		double interest = control.getBalance() * rate / 100;
	            control.deposit(interest);
	            resultLabel.setText("balance: " + control.getBalance());
		}
	}			
	public String getRateField(){
		return rateField.getText();
	}	
	public JLabel getResultLabel(){
		return resultLabel;
	}
}
